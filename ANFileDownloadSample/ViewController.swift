//
//  ViewController.swift
//  ANFileDownloadSample
//
//  Created by Ahmed Nasser on 2/28/18.
//  Copyright © 2018 Ahmed Nasser. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var doneIconImageView: UIImageView!
    @IBOutlet weak var pauseButton: UIButton!
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var previewImageView: UIImageView!
    var fileRequestor = FileRequestor()
    var isDownloading = false
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    let animationDuration = 1.0
    func hideDownloadButton()  {
        self.downloadButton.isHidden = true
        self.activityIndicator.isHidden = false
        self.doneIconImageView.isHidden = true
        self.pauseButton.isEnabled = true
        
    }
    
    func showDownloadButton()  {
        self.downloadButton.isHidden = false
        self.activityIndicator.isHidden = true
        self.doneIconImageView.isHidden = true
        self.pauseButton.isEnabled = true
    }
    
    func showDoneIcon()  {
        self.downloadButton.isHidden = true
        self.activityIndicator.isHidden = true
        self.doneIconImageView.isHidden = false
        self.pauseButton.isEnabled = false
    }
    
    @IBAction func resetButtonTouched(_ sender: Any) {
        // flipping the reset button animation
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.resetButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        UIView.animate(withDuration: 0.5, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            self.resetButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)
        }, completion: nil)
        
        fileRequestor.cancelDownload()
        fileRequestor = FileRequestor()
        previewImageView.image = nil
        isDownloading = false
        progressBar.progress = 0
        showDownloadButton()
    }
    
    @IBAction func pauseButtonTouched(_ sender: Any) {
        fileRequestor.pauseDownload()
        downloadButton.isHidden = false
        activityIndicator.isHidden = true
    }
    
    @IBAction func downloadButtonTouched(_ sender: Any) {
        hideDownloadButton()
        if isDownloading {
            fileRequestor.resumeDownload()
        }else{
            isDownloading = true
            let url = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Vincent_van_Gogh_-_De_slaapkamer_-_Google_Art_Project.jpg/4096px-Vincent_van_Gogh_-_De_slaapkamer_-_Google_Art_Project.jpg"
            
            fileRequestor.downloadImage(url: url,filename: "test.png",onCompletion: { (image) in
                self.previewImageView.image = image
                self.showDoneIcon()
                self.progressBar.progress = 1.0 // setting the progressbar full in case the download completed while app is on background
            },onProgress: {progressFraction in
                print(progressFraction)
                self.progressBar.setProgress(Float(progressFraction), animated: true)
            },onFailure: {error in
                
                
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

