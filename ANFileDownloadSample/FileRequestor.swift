//
//  FileRequestor.swift
//  ANFileDownloadSample
//
//  Created by Ahmed Nasser on 2/28/18.
//  Copyright © 2018 Ahmed Nasser. All rights reserved.
//

import UIKit
import Alamofire
class FileRequestor {
    private var image: UIImage?

    private var request:DownloadRequest?
    func pauseDownload()
    {
        request?.suspend()
    }
    func cancelDownload()
    {
        request?.cancel()
    }

    func resumeDownload()
    {
        request?.resume()
    }
    
    func downloadImage(url: String, filename: String, onCompletion: @escaping (UIImage?) -> Void, onProgress: @escaping (Double) -> Void, onFailure: @escaping (Error) -> Void) {
        guard image == nil else { onCompletion(image) ; return }

        let destination : DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(filename)

            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        request = BackgroundSessionManager.shared.download(url,to:destination)
        
        request?.downloadProgress { (progress) in
            onProgress(progress.fractionCompleted)
        }
        request?.responseData { response in
            switch response.result {
            case .success(let data):
                self.image = UIImage(data: data)
                onCompletion(self.image)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
}
