//
//  SessionManager.swift
//  ANFileDownloadSample
//
//  Created by Ahmed Nasser on 3/1/18.
//  Copyright © 2018 Ahmed Nasser. All rights reserved.
//

import UIKit
import Alamofire

class BackgroundSessionManager: NSObject {
    // MARK: - Properties
    
    static let shared : SessionManager = {
        let configuration = URLSessionConfiguration.background(withIdentifier: "com..ANFileDownloadSample.Background")
        return SessionManager(configuration: configuration)
    }()
    
    // Initialization
    
    private override init() {
    }
    
}
