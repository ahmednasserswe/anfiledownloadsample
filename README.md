# ANFileDownloadSample
## Sample app for using Alamofire for downloading files while supporting pause, resume, and background session
The app tries the concept by downloading an image file and showing it to validate the download.
Test by: 
1) Pause and resume 
2) Move the app to the background and notice if the download still in progress. 


----
Icons were downloaded from icons8.com
Test image from Wikimedia https://commons.wikimedia.org/wiki/File:Vincent_van_Gogh_-_De_slaapkamer_-_Google_Art_Project.jpg